# Установка vault на сервер
### Запустить плейбук
`ansible-playbook -i vault_server.inv install_vault.yml`
### Зайти на http://<SERVER IP>:8200/ui и произвести первоначальную установку 
После окончания установки vault отдаст корневой токен и ключи (никогда не используйте 1 ключ для кластера)

Используемые ключи для теста:
**root:** `s.lorFAzl9J7chf5YBpSM7OWXd`
**key 1:** `wZCHXOsyVbZ+X0iF9LdISaFqZKHgeVPdClHdh4LTOLg=`